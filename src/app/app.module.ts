import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChechComponent } from './chech/chech.component';

import { LoginComponent } from './user/login/login.component';
import{UserModule}from'./user/user.module';

@NgModule({
  declarations: [
    AppComponent,
    ChechComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UserModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
