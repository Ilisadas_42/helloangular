import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChechComponent } from './chech.component';

describe('ChechComponent', () => {
  let component: ChechComponent;
  let fixture: ComponentFixture<ChechComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChechComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
